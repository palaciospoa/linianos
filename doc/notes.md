#Usage

>note: phpunit 7.1 required

Install packages
````
composer install
````

Launch using
```
php app.php
```

Test using
```
phpunit
```
For individual test
````
phpunit test/LinianosTest
phpunit test/LinianosV2Test
````


##Difference of versions

In the first version we don't use a single "if", this is for easy  a conceptualization over instructions, but we use a lot of memory because of the storage on the arrays creation, because of that we can not call test over with high numbers 7777777 as example.

In the second version we have decoupled the transformation number to text so we can call a specific number and have a code with high performance and even better to test.

If you test v1 with high numbers you will get something like this:  


>PHP Fatal error:  Allowed memory size of 536870912 bytes exhausted (tried to allocate 268435464 bytes) in /home/vagrant/code/job_test/linianos/src/Linianos.php on line 28

>Fatal error: Allowed memory size of 536870912 bytes exhausted (tried to allocate 268435464 bytes) in /home/vagrant/code/job_test/linianos/src/Linianos.php on line 28