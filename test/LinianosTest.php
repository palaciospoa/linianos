<?php

declare(strict_types = 1);

namespace OmarPalacios\FizzBuzzDemo\Test;

use PHPUnit\Framework\TestCase;
use OmarPalacios\Linianos\Linianos;

final class LinianosTest extends TestCase
{
    /** @var Linianos $linianos */
    private $linianos;

    protected function setUp()
    {
        parent::setUp();
        $this->linianos = new Linianos();
    }

    /**
     * @test
     * @dataProvider dataProvider
     */
    public function it_should_send_a_valid_response(int $actual, $expected):void
    {
        $array=$this->linianos->__invoke(1,100);
        $this->assertSame($expected, $array[$actual]);
    }
    /**
     * @test
     * @dataProvider dataProviderOverrideFive
     */
    public function it_should_override_five(int $actual, $expected):void
    {
        $array=$this->linianos->__invoke(1,100);
        $this->assertNotSame($expected, $array[$actual]);
    }
    /**
     * @test
     * @dataProvider dataProviderOverrideThree
     */
    public function it_should_override_three(int $actual, $expected):void
    {
        $array=$this->linianos->__invoke(1,100);
        $this->assertNotSame($expected, $array[$actual]);
    }

    public function dataProvider():array{
        return [
            "When is 1"  => ["actual" =>  1, "expected" => 1],
            "When is 3"  => ["actual" =>  3, "expected" => 'Linio'],
            "When is 5"  => ["actual" =>  5, "expected" => 'IT'],
            "When is 15" => ["actual" => 15, "expected" => 'Linianos'],
        ];

    }

    public function dataProviderOverrideFive():array{
        return [
            "When is 15"  => ["actual" =>  15, "expected" => 'IT'],
            "When is 30"  => ["actual" =>  30, "expected" => 'IT'],
            "When is 45"  => ["actual" =>  45, "expected" => 'IT'],
        ];
    }

    public function dataProviderOverrideThree():array{
        return [
            "When is 15"  => ["actual" =>  15, "expected" => 'Linio'],
            "When is 30"  => ["actual" =>  30, "expected" => 'Linio'],
            "When is 45"  => ["actual" =>  45, "expected" => 'Linio'],
        ];
    }
}
