<?php

declare(strict_types = 1);

namespace OmarPalacios\FizzBuzzDemo\Test;

use PHPUnit\Framework\TestCase;
use OmarPalacios\Linianos\LinianosV2;

final class LinianosV2Test extends TestCase
{
    /** @var LinianosV2 $linianos */
    private $linianosV2;

    protected function setUp()
    {
        parent::setUp();
        $this->linianosV2 = new LinianosV2();
    }

    /**
     * @test
     * @dataProvider dataProvider
     */
    public function it_should_send_a_valid_response(int $actual, string $expected):void
    {
        $linianosVariationText=$this->linianosV2->transformNumberToLinianosVariationText($actual);
        $this->assertSame($expected, $linianosVariationText);
    }
    /**
     * @test
     * @dataProvider dataProviderOverrideFive
     */
    public function it_should_override_five(int $actual, string $expected):void
    {
        $linianosVariationText=$this->linianosV2->transformNumberToLinianosVariationText($actual);
        $this->assertNotSame($expected, $linianosVariationText);
    }
    /**
     * @test
     * @dataProvider dataProviderOverrideThree
     */
    public function it_should_override_three(int $actual, $expected):void
    {
        $linianosVariationText=$this->linianosV2->transformNumberToLinianosVariationText($actual);
        $this->assertNotSame($expected, $linianosVariationText);
    }

    /**
     * @test
     * @dataProvider dataProviderHighNumbers
     */
    public function it_should_send_a_valid_response_on_high_numbers(int $actual, string $expected):void
    {
        $linianosVariationText=$this->linianosV2->transformNumberToLinianosVariationText($actual);
        $this->assertSame($expected, $linianosVariationText);
    }

    public function dataProvider():array{
        return [
            "When is 1"  => ["actual" =>  1, "expected" => 1],
            "When is 3"  => ["actual" =>  3, "expected" => 'Linio'],
            "When is 5"  => ["actual" =>  5, "expected" => 'IT'],
            "When is 15" => ["actual" => 15, "expected" => 'Linianos'],
        ];

    }

    public function dataProviderOverrideFive():array{
        return [
            "When is 15"  => ["actual" =>  15, "expected" => 'IT'],
            "When is 30"  => ["actual" =>  30, "expected" => 'IT'],
            "When is 45"  => ["actual" =>  45, "expected" => 'IT'],
        ];
    }

    public function dataProviderOverrideThree():array{
        return [
            "When is 15"  => ["actual" =>  15, "expected" => 'Linio'],
            "When is 30"  => ["actual" =>  30, "expected" => 'Linio'],
            "When is 45"  => ["actual" =>  45, "expected" => 'Linio'],
        ];
    }

    public function dataProviderHighNumbers():array{
        return [
            "When is 7777777"  => ["actual" =>  7777777, "expected" => 7777777],
            "When is 3333333"  => ["actual" =>  3333333, "expected" => 'Linio'],
            "When is 5555555"  => ["actual" =>  5555555, "expected" => 'IT'],
            "When is 151515"   => ["actual" =>   151515, "expected" => 'Linianos'],
        ];
    }
}
