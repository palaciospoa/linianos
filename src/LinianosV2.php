<?php

declare(strict_types = 1);

namespace OmarPalacios\Linianos;

final class LinianosV2
{
    public function __invoke($startNumber, $endNumber): void
    {
        for($number=$startNumber;$number<=$endNumber;$number++){
            echo $this->transformNumberToLinianosVariationText($number).PHP_EOL;
        }
    }

    public function transformNumberToLinianosVariationText($number): string{
        $elementsArray = [
            'bothElement'   => ['number' => 15, 'text' => 'Linianos'],
            'secondElement' => ['number' =>  5, 'text' => 'IT'],
            'firstElement'  => ['number' =>  3, 'text' => 'Linio',],
        ];

        foreach ($elementsArray as $element){
            if ($number%$element['number']===0) {
                return $element['text'];
            }
        }
        return ''.$number;
    }
}