<?php

declare(strict_types = 1);

namespace OmarPalacios\Linianos;

final class Linianos
{
    public function __invoke($startNumber, $endNumber):array
    {
        $firstElement  = ['number' =>  3, 'text' => 'Linio'];
        $secondElement = ['number' =>  5, 'text' => 'IT'];
        $bothElement   = ['number' => 15, 'text' => 'Linianos'];

        $number             = $firstElement['number'];
        $text               = $firstElement['text'];
        $firstArrayElements = $this->getArrayWithTextElement($text, $number, $endNumber);

        $number              = $secondElement['number'];
        $text                = $secondElement['text'];
        $secondArrayElements = $this->getArrayWithTextElement($text, $number, $endNumber);

        $number            = $bothElement['number'];
        $text              = $bothElement['text'];
        $bothArrayElements = $this->getArrayWithTextElement($text, $number, $endNumber);

        $baseArray = range($startNumber, $endNumber);
        $baseArray = array_combine($baseArray, $baseArray);

        $arrayMix = array_replace($baseArray, $firstArrayElements, $secondArrayElements, $bothArrayElements);

        return $arrayMix;
    }

    private function getArrayWithTextElement($text, $jump, $end): array
    {
        $array = [];
        for ($i = $jump; $i <= $end; $i += $jump) {
            $array[$i] = $text;
        }
        return $array;
    }

}