<?php

declare(strict_types = 1);

use OmarPalacios\Linianos\Linianos;
use OmarPalacios\Linianos\LinianosV2;

require_once __DIR__ . '/vendor/autoload.php';

$linianos = new Linianos();
$arrayLinianos=$linianos(1,100);

echo "********************".PHP_EOL;
echo "*** Linianos v1 ****".PHP_EOL;
echo "********************".PHP_EOL;
foreach ($arrayLinianos as $key=>$value){
    echo "$key => $value".PHP_EOL;
};

echo "********************".PHP_EOL;
echo "*** Linianos v2 ****".PHP_EOL;
echo "********************".PHP_EOL;

$linianosV2 = new LinianosV2();

$linianosV2(1,100);
